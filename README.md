﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS
## Sesión 1: Programación en Java

Antes de empezar con la biblioteca de Java para la programación concurrente se revisarán conceptos básicos sobre programación Java que serán de utilidad para el desarrollo correcto de las siguientes sesiones y prácticas de la asignatura.

Los **Objetivos** de esta práctica son los siguientes:

- Dejar claro el estilo de programación que deberá utilizarse en el desarrollo de las prácticas en Java. Para ello se utilizará el entorno de desarrollo IntelliJ.
- Crear una primera clase en Java y aprender a incluir código autogenerado en IntelliJ para el constructor, métodos de acceso y métodos de utilidad de la clase.
- Entender la utilidad de la clase que incluye el método `main()`. Esta será la clase que representa el hilo principal para el resto de las prácticas.
- Crear y utilizar constantes en el proyecto que permitan una mejor legibilidad del código.
- Dividir la solución del problema en módulos para que la legibilidad del código sea mayor. Esto ayudará a comprender la necesidad de utilizar módulos en prácticas posteriores y así ayudar a la legibilidad de los hilos presentes en el proyecto.

## 1. Introducción a la Programación Java

Se presentará paso a paso el desarrollo de un proyecto de ejemplo para lograr los objetivos que nos hemos propuesto alcanzar en este guión de prácticas:

>Se simulará la ejecución de un gestor de impresión que recibirá diferentes trabajos que deberá asignar a las impresoras que tiene disponibles. Se dispondrá una cantidad de impresoras que no cambiará con la ejecución del problema. El número de trabajos de impresión no estará limitado pero el tiempo necesario para completarlos será aleatorio entre **2 y 4 segundos**. Con una probabilidad del **70%** se añadirá un nuevo trabajo de impresión al gestor y un **30%** para atender el primer trabajo de impresión pendiente. El gestor finalizará su tarea cuando haya completado un número de trabajos de impresión fijados previamente.

### Crear el proyecto

El primer paso será crear el proyecto IntelliJ, una vez que ejecutemos el entorno de programación pulsaremos el botón *New Project*.

![Nuevo Proyecto][imagen1]

Configuramos el proyecto con el nombre `Sesion1` y revisar bien el directorio donde se almacenará el proyecto, el directorio dependerá del ordenador donde se crea al proyecto. Antes de finalizar elegimos como **Gropup Id:** `es.uja.ssccdd` y ya tendremos la configuración inicial para nuestra aplicación Java. Pulsamos el botón finalizar y así se completa el asistente.

![Crear Aplicación Java][imagen2]

Antes de pasar al desarrollo del código necesario para el hilo principal completamos las clases necesarias para ello.

### Crear Clase `Impresora`

Esta clase nos permitirá dar una representación a las impresoras presentes en el sistema para realizar una simulación del gestor de impresión. Para ello utilizaremos el asistente para añadir un nuevo archivo al proyecto que represente una clase Java, pinchamos con el botón derecho del ratón sobre el paquete del proyecto y seleccionamos **Clase Java** y pulsamos el botón siguiente.

![Asistente para Ficheros Java][imagen3]

Como nombre de la clase utilizamos `Impresora`y la clase se creará en el paquete que inicialmente se había seleccionado.

![Nueva Clase Java][imagen4]

Una vez que ha finalizado el asistente tenemos creada la clase Java:

```java
package es.uja.ssccdd;

public class Impresora {
    
}
```
El siguiente paso será la definición para las variables de instancia de la clase, esto es lo primero que haremos siempre que creemos una clase, y las ubicaremos justo después del nombre de la clase. Para esta clase solo necesitaremos una variable de instancia que nos permita diferenciar las impresoras presentes en el sistema. La variable será entera: `private final int id;` Las variables de instancia las declararemos siempre privadas a la clase. En este caso, además, utilizamos el modificador `final` porque no vamos a cambiar el identificador de la impresora, una vez haya sido creada, y así se comportará como una constante.

Para crear el constructor de la clase pinchamos con el botón derecho para desplegar el menú contextual y seleccionamos la inserción de código y luego **Constructor**. Debemos asegurarnos que tenemos seleccionada las variables de instancia que formarán parte de los parámetros del constructor. Todas las variables de instancia tendrán que inicializarse en el constructor, aunque no formen parte de los parámetros, y solo inicializaremos las variables de instancia, es decir, no añadiremos más código al constructor.

![Crear Constructor Clase][imagen5]

El resultado final debería ser el que se muestra:

```java
public Impresora(int id) {
    this.id = id;
    }
```

Como la variable de instancia tiene un ámbito privado a la clase necesitaremos un método de acceso para poder conocer su valor. Para ello volvemos a pinchar con el botón derecho del ratón y seleccionamos la inserción de código. De los métodos de acceso solo podemos seleccionar **Getter**, si no fuera una constante seleccionaremos los **Getter y Setter**, y nos aseguraremos que hemos seleccionado las variables de instancia.

![Crear Métodos Acceso][imagen6]

Para finalizar, volveremos a pinchar con el botón derecho para insertar código. Ahora seleccionamos **toString()**. Que nos dará una representación en formato texto de un objeto de la clase. No debemos olvidar seleccionar las variables de instancia presentes en la Clase.

![Crear Método toString][imagen7]

Podemos personalizar el método que nos genera para adaptarlo a nuestras necesidades. El resultado final de la clase `Impresora` debe ser algo similar a lo que se muestra: 

```java
package es.uja.ssccdd;

public class Impresora {
    private final int id;

    public Impresora(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override  
	public String toString() {  
	    return "Impresora{" +  
	            "id=" + id +  
	            '}';  
    }
}
```

### Crear Clase `TrabajoImpresion`

Creamos una clase que nos permita dar una representación a los trabajos de impresión que deberá completar el gestor de impresión. Se crea de forma similar a la clase `Impresora` utilizando como nombre `TrabajoImpresion`. Como variable de instancia incluimos la siguiente:

- `private final int idTrabajo;`
- `private final int tiempoTrabajo;`

Creamos el constructor, métodos de acceso y el método `toString()` de forma análoga a lo que ya hemos hecho para la clase `Impresora`

### Crear Clase `GestorImpresion`

Ahora vamos a crear la clase que se encargará de simular las acciones del gestor de impresión. Volvemos a utilizar el asistente para añadir un nuevo archivo de clase Java al proyecto, el nombre será `GestorImpresion`.

Las variables de instancia serán las siguientes:

- `private final Impresora[] impresoras;`
- `private final ArrayList<TrabajosImpresion> trabajos;` 

Como se dice en el enunciado del ejercicio, las impresoras en el sistema están definidas y no cambian. Esto quiere decir que podemos utilizar un array para representar las impresoras en el sistema. Un array en Java se representa con los `[]` que estarán pegados al identificador de un tipo base o una clase de Java. En nuestro ejemplo al identificador de la clase `Impresora`.

Los trabajos de impresión no tienen un tamaño fijo (este es desconocido y crece en tiempo de ejecución). Es por eso que utilizaremos la clase `ArrayList<>` cuya implementación del tipo lista nos permite acceder a los elementos como si fueran los de un array. 

```java
public class GestorImpresion {
    private final Impresora[] impresoras;
    private final ArrayList<TrabajoImpresion> trabajos;
    
}
```

En este momento se nos presentan dos notificaciones, la primera es porque no tenemos inicialización para la variable `impresoras`, que se solucionará cuando creemos el constructor, y la segundo incluye una indicación. Nos está indicando que no conoce el identificador `ArrayList`. Si pinchamos con el ratón nos muestra un menú que nos permite añadir la definición de `ArrayList` del paquete de bibliotecas que conoce Netbeans. Revisar con cuidado si tenemos diferentes opciones, porque tengamos diferentes definiciones para una clase en concreto, aunque no es el caso en este ejemplo. Seleccionamos añadir la definición de la clase y se incluirá de forma automática la siguiente sentencia al archivo Java: 

```java
import java.util.ArrayList;
```

Ahora ya podemos crear el constructor de forma similar a como se ha hecho con la clase `Impresora`. Recordar que hay que seleccionar todas las variables de instancia. El código resultante debe ser como el que se muestra: 

```java
public GestorImpresion(Impresora[] impresoras, ArrayList<TrabajoImpresion> trabajos) {
    this.impresoras = impresoras;
    this.trabajos = trabajos;
    }
```

No vamos a crear métodos de acceso porque no es necesario acceder a las variables de instancia. Pero vamos a necesitar representar las acciones que tiene que hacer esta clase por medio de métodos propios

#### Agregar un trabajo de impresión

Añadimos un nuevo `TrabajoImpresion` a la lista de trabajos, para ello creamos un método llamado `agregarTrabajo(.)` . Debemos consultar en la documentación los métodos que tenemos a nuestra disposición de la clase `ArrayList` y elegimos uno de los que permiten añadir un elemento a la lista. El que se va a utilizar es el método `add(.)`. Con esto podemos completar el código necesario para realizar la acción descrita:

```java
/**
 * Nos permite añadir un nuevo trabajo de impresión a la lista de trabajos
 * pendientes.
 * @param elm el trabajo de impresión que queda pendiente en el gestor 
 */
public void agregarTrabajo ( TrabajoImpresion  elm ) {
    trabajos.add(elm);
        
    System.out.println("Se a añadido el trabajo " + elm + " para su impresión");
}
```

En el código podemos observar la utilidad de haber definido el método `toString(.)` en la clase `TrabajoImpresion` para poder presentar en la consola una representación más legible que ayuda a limpiar el código y así aumentar la legibilidad.	

También quiero señalar que es conveniente añadir un comentario con el formato **Javadoc** a los métodos que se creen. El editor nos ayudará a ello, primero completamos el código del método. Cuando ya estemos seguros que nuestro código funciona, posicionamos el cursor en la línea superior a la cabecera el método e insertamos el texto: `/**` y pulsamos **Enter**. El editor crea la plantilla del comentario y ya podremos personalizarlo.

#### Realizar un trabajo de impresión

Este método nos permitirá simular la realización de uno de los trabajos de impresión pendientes:

```java
/**
 * Simula la realización de un trabajo de impresión en una de las impresoras
 * disponibles en el sistema.
 * @throws java.lang.InterruptedException
 */
public void realizarTrabajo() throws InterruptedException {
    int idImpresora;
    TrabajoImpresion siguienteTrabajo;
               
    idImpresora = seleccionarImpresora();
    siguienteTrabajo = trabajos.remove(PRIMERO);
    TimeUnit.SECONDS.sleep(siguienteTrabajo.getTiempoTrabajo());
        
    System.out.println("Se ha realizado el trabajo: " + siguienteTrabajo + 
            " en la impresora: " + impresoras[idImpresora]);
}
```

Lo primero que debemos hacer es declarar las variables que utilizaremos a lo largo del método. Es importante que se declaren las variables al inicio del método para aumentar la legibilidad del código. 

Para simular la realización del trabajo de impresión debemos tener presente los siguientes pasos:

- Seleccionar una impresora disponible en el sistema. Para ello necesitaremos conocer el identificador de la impresora. Podemos observar que utilizaremos el método `seleccionarImpresora()` para obtener ese identificador. Este método lo presentaré más adelante, pero es importante que creemos este método por si el modo de selección de la impresora pueda cambiar en algún momento. Además mejora mucho la legibilidad del código.

- Seleccionamos el primer trabajo disponible para simular su realización. Al estar utilizando la clase `ArrayList` para representar la lista de trabajos pendientes el método necesario será `remove(indice)`. Para evitar utilizar el valor `0` para indicar que es el primer elemento disponible, y aumentar la legibilidad, definiremos una constante, que colocaleremo justo debajo de la definición de las variables de instancia, que tiene la siguiente sintaxis: `private static final int PRIMERO = 0;` Para identificar claramente las constantes en el código su identificador estará en mayúsculas.

- Para simular tiempo de trabajo, esto se repetirá a lo largo de las prácticas siempre que queramos simular tiempo, utilizaremos la clase `TimeUnit` y el método `sleep(.)`. La clase tiene diferentes unidades de tiempo para saber el tiempo que queremos simular. La ejecución de nuestro método se suspenderá por el tiempo que se haya indicado en el método `sleep(.)`. El editor nos dará una advertencia, posterior a añadir la definición de la clase `TimeUnit`, porque el método tiene una interrupción programada definida dentro de Java. El el siguiente guión se tratarán más en detalle el tema de las interrupciones y por ahora seleccionamos la opción de **Añadir cláusula `throws…`** de las posibilidades que se nos presenta.

Por último solo nos queda presentar un mensaje para que nos quede claro que se ha realizado la simulación del trabajo de impresión.

#### Saber si hay trabajos pendientes

Para garantizar que hay trabajos pendientes debemos crear un método para que el sistema pueda preguntarle al gestor antes de pedir que realice un trabajo de impresión.

```java
/**
 * Nos devuelve si hay trabajos pendientes en el gestor de impresión
 * @return true si hay trabajos pendientes, false en otro caso
 */
public boolean hayTrabajos() {
    return !trabajos.isEmpty();
}
```

#### Seleccionar una impresora disponible

Este será un método privado para el gestor de impresión para que obtenga el identificador de la primera impresora disponible. Como en el ejemplo no estamos trabajando con diferentes hilos, para que podamos tener peticiones concurrentes, seleccionaremos al azar una de las impresoras presentes en el gestor.

```java
/**
 * Seleccionamos una impresora disponible 
 * @return id de la impresora disponible
 */
private int seleccionarImpresora() {
    return generador.nextInt(impresoras.length);
}
```

Para generar un número aleatorio utilizaremos la clase `Random` de Java. El gestor de números aleatorios los definiremos como una variable de instancia de la clase. Pero, a diferencia de las variables que se presentaron anteriormente, no lo vamos a incluir como parámetro en el constructor de la clase, pero sí se inicializa en el constructor. 

Modificamos el constructor de la clase añadiendo la siguiente línea al final: `generador = new Random();`; siempre que queramos obtener una referencia de un objeto de una clase utilizaremos la sentencia `new` y un constructor de una clase, en este caso `Random()`. Recordad que todas las variables de instancia se inicializan en el constructor. No se inicializan las variables de instancia en la definición, esto solo estará permitido para las constantes. También recordad que ningún otro código que no sea necesario para la inicialización de las variables de instancia estará permitido en el contructor, como norma de estilo para las prácticas.

### Crear Clase `Sesion1`

Si al crear el proyecto seleccionamos la opción para que incluyera la clase principal solo tenemos que cambiar el nombre a la clase para que sea `Sesion1`. Si no lo hemos hecho podemos crear una nueva clase, como se ha mostrado anteriormente, y al escribir `main..` en el editor podemos autocompletar con el asistente y se creará el método correcto para que esa clase represente al hilo principal.

En una aplicación Java hay, al menos, una clase pública tiene un método definido de la siguiente forma: 

```java
public static void main(String[] args) {.} 
```

Este método representa la ejecución del hilo principal para las prácticas en las siguientes sesiones. Esta clase será especial, no incluiremos constructor, y completamos el código necesario para el método `main(.)`. También añadiremos métodos privados para aumentar la legibilidad el método `main(.)`, este estilo de programación lo trasladaremos posteriormente en el desarrollo de los métodos que representen los hilos secundarios en las sucesivas prácticas.

Como primer paso, que también repetiremos para otras prácticas, será definir las constantes que necesitaremos en el desarrollo del ejercicio propuesto. En nuestro caso las siguientes:

```java
// Constantes
public static final int MAX_TRABAJOS_REALIZADOS = 10;
public static final int NUM_IMPRESORAS = 3;
public static final int MIN_TIEMPO_TRABAJO = 2;
public static final int MAX_TIEMPO_TRABAJO = 3;
public static final float PROB_REALIZAR_TRABAJO = 0.3f;
```

La definición de las constantes para la realización de las prácticas es una de las normas de estilo que se deberá seguir y será una causa de penalización para la evaluación de los ejercicios y prácticas de la asignatura. El identificador para las constantes debe ser lo suficientemente claro para garantizar un código auto explicativo, otra de las normas de estilo que se deberá aplicar al código que se desarrolle en las prácticas.

#### Completando el método `main(.)`

Como primer paso para completar el código del método 'main(.)' será definir las variables que serán necesarias para el desarrollo correcto del hilo principal. Para nuestro ejercicio necesitaremos definir las impresoras, la lista de trabajos, el gestor de impresión y un generador de números aleatorios que utilizará el sistema. Lo siguiente será un mensaje que indique que el hilo principal ha iniciado su ejecución. Posteriormente, cuando trabajemos con más hilos, los mensajes que se presenten a la consola deberán identificar claramente el hilo que los realiza:

```java
// TODO code application logic here
Impresora[] impresoras;
ArrayList<TrabajoImpresion> trabajos;
GestorImpresion gestor;
Random generador;
        
System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
```

Se inicializan las variables:

```java
// Inicialización de las variables
impresoras = new Impresora[NUM_IMPRESORAS];
for (int i = 0; i < NUM_IMPRESORAS; i++) {
    Impresora nuevaImpresora = new Impresora(i);
    impresoras[i] = nuevaImpresora;
}
        
trabajos = new ArrayList();
gestor = new GestorImpresion(impresoras, trabajos);
        
generador = new Random();
```

El bloque de operación del hilo principal será el siguiente:

```java
// Trabajo con el gestor hasta completar el número de trabajos previsto
int completados = 0;
int idTrabajo = 0;
while ( completados < MAX_TRABAJOS_REALIZADOS ) {
    if ( (generador.nextFloat() < PROB_REALIZAR_TRABAJO) && gestor.hayTrabajos() ) {
        // Se completa un trabajo de impresión
        completados++;
        try {
            // Tratamos la interrupción por defecto
            gestor.realizarTrabajo();
            System.out.println("Hilo(Principal) se han completado " + completados +
                            " trabajos");
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion1.class.getName()).log(Level.SEVERE, null, ex);
        }
    } else {
        // Agregamos un nuevo trabajo al gestor que queda pendiente para
        // atenderlo más adelante
        idTrabajo++;
        int tiempo = MIN_TIEMPO_TRABAJO + generador.nextInt(MAX_TIEMPO_TRABAJO);
        TrabajoImpresion nuevoTrabajo = new TrabajoImpresion(idTrabajo, tiempo);
        gestor.agregarTrabajo(nuevoTrabajo);
    }
}
```

Como se pide en el problema, terminamos cuando se alcance un número de trabajos fijado. El gestor de trabajos tendrá que realizar el primero, si hay disponibles, cuando se cumpla una condición. Como solo estamos utilizando un único hilo, el gestor realiza un trabajo o añade un trabajo, solo puede atender una de las posibilidades que tiene el gestor de impresión.

Cuando atendemos un trabajo e incluimos el código: `gestor.realizarTrabajo();`; vemos que nos aparecerá una indicación en el editor, como ya nos pasó anteriormente con el método `sleep(.)`, porque tenemos que atender a una interrupción. Ahora seleccionamos la opción de **Encerrar sentencia con `try-catch..`**. Esto quiere decir que queremos tratar la interrupción ahí, si se produce, pero dejaremos el tratamiento por defecto. En el siguiente guión se tratará en detalle este tema.

```java
try {
    // Tratamos la interrupción por defecto
    gestor.realizarTrabajo();
    System.out.println("Hilo(Principal) se han completado " + completados +
                    " trabajos");
} catch (InterruptedException ex) {
    Logger.getLogger(Sesion1.class.getName()).log(Level.SEVERE, null, ex);
}
```

Solo nos queda para finalizar el método incluir un mensaje a la consola que nos indica que el hilo principal ha terminado. Esto también deberemos realizarlo en los hilos secundarios que crearemos en otras prácticas.

```java
System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
```

Como hemos visto, el hilo principal, se ha dividido en diferentes bloques para ayudar a su comprensión. En posteriores prácticas deberéis dividir en módulos, implementados como métodos privados de la clase, los bloques que os aparezcan en el método que ejecuta el código de cada uno de los hilos secundarios. Esto ayudará a una mejor comprensión de lo que hace en cada momento en el hilo secundario y, además, ayudará a una mejor gestión de las interrupciones en el código de los hilos secundarios. En las resoluciones de prácticas de cursos anteriores podéis tener ejemplo de esta modularización. También se seguirá este estilo en las soluciones de las sesiones de prácticas de este curso académico.

[proyecto]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/nuevoProyecto.jpg
[archivo]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/nuevoFichero.jpg 
[imagen1]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/imagen1.jpg "Nuevo Proyecto"
[imagen2]: https://gitlab.com/ssccdd/guionsesion1/-/raw/master/img/imagen2.jpg "Crear Aplicación Java"
[imagen3]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/imagen3.jpg "Asistente para Ficheros Java"
[imagen4]: https://gitlab.com/ssccdd/guionsesion1/-/raw/master/img/imagen4.jpg "Nueva Clase Java"
[imagen5]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/imagen5.jpg "Crear Constructor Clase"
[imagen6]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/imagen6.jpg "Crear Métodos Acceso"
[imagen7]: https://gitlab.com/ssccdd/guionsesion1/raw/master/img/imagen7.jpg "Crear Método toString()"
